### Description

<!-- Add a textual description of the security issue. -->

### Steps to reproduce

<!-- Add a step by step guide describing how to trigger this issue. -->

1.

<!-- Remove the labels that don’t apply. -->

/label ~App ~Block ~CLI ~Docker ~Editor ~Server

/label Security

/estimate 1d

/confidential
