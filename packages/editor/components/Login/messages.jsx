import { defineMessages } from 'react-intl';

export default defineMessages({
  title: 'Login',
  login: 'Login with {provider}',
  registerLink: 'Register a new account',
  forgotPasswordLink: 'Forgot your password?',
});
