import { defineMessages } from 'react-intl';

export default defineMessages({
  edit: 'Edit',
  icon: 'Icon',
  view: 'View',
});
