import { defineMessages } from 'react-intl';

export default defineMessages({
  editor: 'Editor',
  resources: 'Resources',
});
