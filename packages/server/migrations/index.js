import initial from './0.6.0-initial';
import user from './0.7.0-user-profile';
import organizations from './0.8.0-organizations';
import invite from './0.8.3-invite';
import uniquePath from './0.8.4-unique-path';
import blockParameters from './0.8.7-block-parameters';
import nullablePath from './0.8.8-nullable-path';

export default [initial, user, organizations, invite, uniquePath, blockParameters, nullablePath];
