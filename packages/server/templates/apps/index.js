import Empty from './empty';
import HolidayApp from './holiday';
import PersonApp from './person';
import UnlitteredApp from './unlittered';

export default [Empty, PersonApp, HolidayApp, UnlitteredApp];
